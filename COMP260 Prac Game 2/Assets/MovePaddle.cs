﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	private Rigidbody rigidbody;
	public float speed = 10f;
	public float acceleration = 1.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second
	public float maxVelocityChange = 10.0f;
	public float force = 10f;



	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;

	}

	void UseVelocity ()
	{
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;
		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;
		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}
		rigidbody.velocity = vel;
	}

	void UseAddForce ()
	{
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rigidbody.position;
		rigidbody.AddForce (dir.normalized * force);
	}

	void UseMixVelocityAndForce(){
		// source: rigidbodyfpswalker
		// Calculate how fast we should be moving
		Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"), 0 );
		//print (targetVelocity);
		//targetVelocity = transform.TransformDirection(targetVelocity);
		targetVelocity *= speed;

		// Apply a force that attempts to reach our target velocity
		Vector3 velocity = rigidbody.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = Mathf.Clamp(velocityChange.y, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = 0;
		rigidbody.AddForce(velocityChange, ForceMode.VelocityChang'e);
			// not finished yet, need answer questions from prac
	}

	void FixedUpdate () {
		

		//UseAddForce ();


		//UseVelocity ();

		UseMixVelocityAndForce ();
	}


	// Update is called once per frame
	void Update () {


	}

	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}

}
